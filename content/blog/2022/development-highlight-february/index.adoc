+++
title = "Development Highlight: February Update"
date = "2022-02-01"
draft = false
"blog/categories" = [
    "Development Highlight"
]
+++

It's only been a month since the 6.0 release and work is well under way for the future 7.0 release with many features and improvements to come over the course of the year.

As a friendly reminder, nightlies are currently unstable, you can use them for testing and experimenting but we make no guarantees on the stability right now. Use them at your own risk, keep backups, especially if you are upgrading nightly versions.

== Release Schedule Change
The KiCad team is planning to switch to annual release targets for major releases with each release happening in January. No promises yet but this is the goal.
Patch fixes for the current stable release will occur as normal over the course of each year while the next major release is worked on.

== DRC Improvements
Immediately following the release of 6.0, https://gitlab.com/jeffyoung[Jeff Young] pushed numerous DRC improvements

=== PCB to Library Footprint Consistency Checking

A new optional check is checking if your board design has footprints that deviated from the library they purport to come from.

image::footprint-library-check.png[align=center, alt="Example of Board to Library Footprint Checking", link=footprint-library-check.png]

=== Ignored Tests Tab

Ignored tests are now listed in their own separate tab.

image::ignored-drc.png[align=center, alt="Ignored Tests tab shown in DRC window", link=ignored-drc.png]

=== Mechanical Clearance Rules.

Two new DRC rule constraints `mechanical_clearance`` and `mechanical_hole_clearance` to the complement the clearance and hole_clearance constraints.

While the later are not run on items of the same net, the mechanical_* versions are.

```
# Prevent solder wicking from SMD pads
(rule holes_in_pads
    (constraint mechanical_hole_clearance (min 0.2mm))
    (condition "B.Pad_Type == 'SMD'"))
```

=== Custom Rule Severities

Severities can now be defined per custom rule with possible values of `ignore`, `warning`, `error`, and `exclusion`.

```
(rule "Via Hole to Track Clearance"  
    (constraint hole_clearance (min 0.254mm))
    (condition "A.Type =='Via' && B.Type =='Track'")
    (severity warning))
```

=== Pad to Zone rules
`thermal_relief_gap`, `thermal_spoke_width` and `zone_connection` now are new constraints added for rule checking copper connections that get generated between pads and zone fills.

`zone_connection` has valid options of solid, thermal_reliefs or, none.

```
    (rule heavy_thermals
       (constraint thermal_spoke_width (min 0.5mm))
       (condition "A.NetClass == 'HV'"))

    # Don't use thermal reliefs on heatsink pads
    (rule heat_sink_pad
        (constraint zone_connection solid)
        (condition "A.Fabrication_Property == 'Heatsink pad'"))
```

== Radial Dimensions
KiCad 6.0 introduced a linear dimension tool. https://gitlab.com/jeffyoung[Jeff Young] in https://gitlab.com/kicad/code/kicad/-/commit/4b6bf3095a3b1984268ca797aea92029ab4d4b63[4b6bf30] has now introduced a new Radial Dimension tool for 7.0.

image::radial-dimensions.png[align=center, alt="Example of radial dimensions", link=radial-dimensions.png]

== Schematic shapes
Support for a simple rectangle and circle primitive has been added to the schematic editor to round out 

image::shapes.png[align=center, alt="Example of radial dimensions", link=shapes.png]

== Custom fonts
link:https://gitlab.com/rockola[Ola Rinta-Koski] laid down a solid chunk of groundwork in https://gitlab.com/kicad/code/kicad/-/merge_requests/613[MR #613] which was later taken in by Jeff Young for more overall restructuring of the codebase to introduce custom font support in both schematic and PCB editors. You can now freely use Comic Sans to your heart's content.

image::custom-font.png[align=center, alt="Example of CJK custom font", link=custom-font.png]

image::wingdings.png[align=center, alt="Example of Wingdings and other oddball fonts", link=wingdings.png]

== Text Boxes

Jeff Young in multiple commits https://gitlab.com/kicad/code/kicad/-/commit/c6a8100d46e0a7cbb77e2f825b563412ca24201c[c6a8100], https://gitlab.com/kicad/code/kicad/-/commit/5739505aa35a8a2349ef34f58d0ef7e643510e72[5739505], https://gitlab.com/kicad/code/kicad/-/commit/f3cd36d1d7689e6047553c0a526c4058a7dcde85[f3cd36d] has added support for text boxes in both the schematic and PCB editors.

image::textboxes.jpg[align=center, alt="Example of CJK custom font", link=textboxes.jpg]

== 3Dconnexion SpaceMouse Support

https://gitlab.com/markus-bonk[Markus Bonk] working for https://3dconnexion.com/[3Dconnexion] has contributed support in the PCB editor and 3D viewer for their line of SpaceMouse products in https://gitlab.com/kicad/code/kicad/-/merge_requests/845[MR #845]. For those unfamiliar, space mouses are 3D navigation capable mice that give a unique way of navigating both 3D and even 2D canvas in one simple device without needing key presses or mouse clicks. Certain power users may find this a welcome expansion to KiCad's capabilities to support their needs.

Currently this integration only works on Windows and macOS where 3dconnexion provides official support. The KiCad team is exploring integration for libspacenav to provide Linux support.


== Last Month's Contributors
The above features weren't the only contributions made to KiCad, we would like to thank all of the contributors since the release of KiCad 6.0 on December 24th, 2021 to February 1st, 2022!

* Jeff Young
* jean-pierre charras
* Seth Hillbrand
* Marek Roszko
* Mike Williams
* Thomas Pointhuber
* markus-bonk
* Wayne Stambaugh
* Jon Evans
* Tomasz Wlostowski
* Ian McInerney
* Mark Roszko
* Ola Rinta-Koski
* Mikolaj Wielgus
* Roberto Fernandez Bautista
* Steffen Mauch
* Frank Zeeman
* Jan Straka
* Mark Hämmerling
* qu1ck
* Arnau Llovet Vidal
* Marco Ciampa
* Steven A. Falco
* taotieren
* Fabien Corona
* Franck Bourdonnec
* Rigo Ligo
* Tian Yunhao
* Tokita, Hiroshi
* Wellington Terumi Uemura
* Axel Henriksson
* Brian Mayton
* Davide Gerhard
* Eric
* Erwan Rouault
* Franck
* Gökhan Koçmarlı
* Jacob Mealey
* Jonathan Haas
* Jose Perez
* Rafael Silva
* Scott Candey
* Simon Richter
* Tom Keddie
* Ulices
* Vesa Solonen
* alexfanqi